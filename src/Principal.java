import java.io.*;
import java.util.Scanner;

public class Principal {

	private static int picWidth;
	private static int picHeight;
	private static int maxValue;

	private static int picWidthC;
	private static int picHeightC;
	private static int maxValueC;

	private static int[][] imatgeDescomprimidaAMatriu(String nomArxiu) throws IOException {
		String filePath = nomArxiu;
		FileInputStream fileInputStream = new FileInputStream(filePath);
		Scanner scan = new Scanner(fileInputStream);
		scan.skip("P5");
		Principal.picWidth = scan.nextInt();
		Principal.picHeight = scan.nextInt();
		Principal.maxValue = scan.nextInt();
		fileInputStream.close();
		scan.close();
		fileInputStream = new FileInputStream(filePath);
		DataInputStream dis = new DataInputStream(fileInputStream);
		int numnewlines = 3;
		while (numnewlines > 0) {
			char c;
			do {
				c = (char) (dis.readUnsignedByte());
			} while (c != '\n');
			numnewlines--;
		}
		int[][] data2D = new int[picHeight][picWidth];
		for (int row = 0; row < picHeight; row++) {
			for (int col = 0; col < picWidth; col++) {
				data2D[row][col] = dis.readUnsignedByte();
			}
		}
		dis.close();
		return data2D;
	}

	private static int[] filaAArray(int fila, int[][] matriu) {
		int[] aux = new int[picWidth];
		for (int i = 0; i < picWidth; i++) {
			aux[i] = matriu[fila][i];
		}
		return aux;
	}

	private static int[][] imatgeComprimidaAMatriu(String nomArxiu) throws IOException {
		FileInputStream fileInputStream = new FileInputStream(nomArxiu);
		Scanner scan = new Scanner(fileInputStream);
		scan.skip("P5");
		picWidthC = scan.nextInt();
		picHeightC = scan.nextInt();
		maxValueC = scan.nextInt();
		fileInputStream.close();
		fileInputStream = new FileInputStream(nomArxiu);
		DataInputStream dis = new DataInputStream(fileInputStream);
		int numnewlines = 3;
		while (numnewlines > 0) {
			char c;
			do {
				c = (char) (dis.readUnsignedByte());
			} while (c != '\n');
			numnewlines--;
		}
		int[][] data2D = new int[picHeightC][picWidthC];
		int row = 0, col = 0, repeticiones = 0, valor = 0, numAux = 0, numAux2;
		boolean continuem = true, continuem2 = true;
		try {
			if (maxValueC == 255) {
				for (row = 0; row < picHeightC; row++) {
					col = 0;
					continuem = true;
					repeticiones = dis.readUnsignedByte();
					while (continuem) {
						valor = dis.readUnsignedByte();
						for (int i = 0; i < repeticiones; i++) {
							data2D[row][col] = valor;
							System.out.println(col);
							col++;
						}
						repeticiones = dis.readUnsignedByte();
						if (repeticiones == 10) {
							if (col == picWidthC) {
								continuem = false;
							} else {
								continuem = true;
							}
						}
					}
				}
			} else {
				for (row = 0; row < picHeightC; row++) {
					System.out.println(row);
					col = 0;
					continuem = true; continuem2=true;
					repeticiones = dis.readUnsignedByte();
					while (continuem) {
						numAux = dis.readUnsignedByte();
						valor = (numAux & 0xF0) >> 4;
						for (int i = 0; i < repeticiones; i++) {
							data2D[row][col] = valor*16;
							col++;
						}
						numAux2 = dis.readUnsignedByte();
						if (numAux2 == 10) {
							if (col == picWidthC) {
								continuem2 = false;
								continuem=false;
							} else {
								continuem2 = true;
							}
						}
						if (continuem2) {
							repeticiones = ((numAux & 0x0F) << 4) | ((numAux2 & 0xF0) >> 4);
							valor = (numAux2 & 0x0F);
							for (int i = 0; i < repeticiones; i++) {
								data2D[row][col] = valor*16;
								col++;
							}
							repeticiones = dis.readUnsignedByte();
							if (repeticiones == 10) {
								if (col == picWidthC) {
									continuem = false;
								} else {
									continuem = true;
								}
							}
						}
					}
				}
			}
		} catch (EOFException e) {
			System.out.println("error");
		}
		return data2D;
	}

	private static void matriuAArxiu(RLE[] arrayRLE, String nomDesti) throws IOException {
		File fout = new File(nomDesti + ".txt");
		FileOutputStream fileOutputStream = new FileOutputStream(fout);
		DataOutputStream dis = new DataOutputStream(fileOutputStream);
		String aux = "P5\n" + picWidth + " " + picHeight + "\n" + maxValue + "\n";
		dis.writeBytes(aux);
		int i = 0;
		for (int k = 0; k < arrayRLE.length; k++) {
			while (arrayRLE[k].getArray(i) != 0) {
				dis.writeByte(arrayRLE[k].getArray(i));
				i++;
				dis.writeByte(arrayRLE[k].getArray(i));
				i++;
			}
			i = 0;
			dis.writeBytes("\n");
		}
		dis.close();
		fileOutputStream.close();
	}

	public static void matriuAArxiuSinRLE(int[][] matriu, String nom) throws IOException {
		File fout = new File(nom + ".pgm");
		FileOutputStream fileOutputStream = new FileOutputStream(fout);
		DataOutputStream dis = new DataOutputStream(fileOutputStream);
		String aux = "P5\n" + picWidthC + " " + picHeightC + "\n" + "255" + "\n";
		dis.writeBytes(aux);

		for (int row = 0; row < picHeightC; row++) {
			for (int col = 0; col < picWidthC; col++) {
				dis.writeByte(matriu[row][col]);
			}
		}

		dis.close();
		fileOutputStream.close();
	}

	public static void comprimirImatge(String nomArxiu) throws IOException {
		int[][] mat = imatgeDescomprimidaAMatriu(nomArxiu);
		RLE matriuRLECodificats[] = new RLE[Principal.picHeight];
		for (int k = 0; k < picHeight; k++) {
			int aux[] = filaAArray(k, mat);
			RLE auxiliar = new RLE(aux);
			auxiliar.codificaRLE();
			matriuRLECodificats[k] = auxiliar;
		}
		String[] aux = nomArxiu.split("\\.");
		matriuAArxiu(matriuRLECodificats, aux[0] + "Comprimit");
	}
	
	public static void divideix16 (int[] array) {
		for(int i=0; i<array.length; i++) {
			array[i]=array[i]/16;
		}
	}

	public static void comprimirImatgeAmbPerdues(String nomArxiu) throws IOException {
		int[][] mat = imatgeDescomprimidaAMatriu(nomArxiu);
		RLE matriuRLECodificats[] = new RLE[Principal.picHeight];
		for (int k = 0; k < picHeight; k++) {
			int aux[] = filaAArray(k, mat);
			divideix16(aux);
			RLE auxiliar = new RLE(aux);
			auxiliar.codificaRLE();
			matriuRLECodificats[k] = auxiliar;
		}
		String[] aux = nomArxiu.split("\\.");
		matriuAArxiuBits(matriuRLECodificats, aux[0] + "Comprimit_4_Bits");
	}

	public static void descomprimirImatge(String nomArxiu, String nomNou) throws IOException {
		int[][] mat = imatgeComprimidaAMatriu(nomArxiu);
		matriuAArxiuSinRLE(mat, nomNou);
	}

	public static void matriuAArxiuBits(RLE[] arrayRLE, String nomDesti) throws IOException {
		File fout = new File(nomDesti + ".txt");
		FileOutputStream fileOutputStream = new FileOutputStream(fout);
		DataOutputStream dis = new DataOutputStream(fileOutputStream);
		String aux = "P5\n" + picWidth + " " + picHeight + "\n" + "15";
		dis.writeBytes(aux);
		/**************************************************
		 * Per tal d'aprofitar la capacitat de la instruccio .writeByte guardarem les
		 * quantitats i els valors d'aquesta manera seguint aquesta sequencia:
		 * byte[0](quantitat) ex. FF 
		 * byte[1](4 bits valor i 4 bits high nibble quantitat) ex. 7F
		 * byte[2](4 bits quantitat low nibble i 4 bits valor) ex. FA
		 * ....... 
		 * Aixo si ho fessim com fins ara quedaria: 0xFF 0x07 0xFF 0x0A 
		 * Aquesta sequencia es torna a repetir exactament igual
		 * *************************************************
		 */
		int i = 0;
		int auxValor = 0;
		int auxQuantitat = 0;
		int auxByte = 0;
		for (int k = 0; k < arrayRLE.length; k++) {
			i = 0;
			dis.writeBytes("\n");
			while (arrayRLE[k].getArray(i) != 0) {
				dis.writeByte(arrayRLE[k].getArray(i)); // 1 byte quantitat
				i++;
				auxValor = arrayRLE[k].getArray(i); // valor
				auxValor = (auxValor << 4) & 0xF0;
				i++;
				auxQuantitat = arrayRLE[k].getArray(i); // quantitat
				if (auxQuantitat == 0) {
					dis.writeByte(auxValor);
					break;
				}
				auxByte = auxQuantitat;
				auxByte = (auxByte & 0xF0) >> 4;
				dis.writeByte(auxValor | auxByte); // 4 bits valor i 4 bits high nibble quantitat
				i++;
				auxValor = arrayRLE[k].getArray(i); // valor
				auxValor = auxValor & 0x0F; // per si de cas
				auxQuantitat = (auxQuantitat << 4) & 0xF0;
				dis.writeByte(auxQuantitat | auxValor); // 4 bits quantitat low nibble i 4 bits valor
				i++;
			}
		}
		dis.writeBytes("\n");
		dis.close();
		fileOutputStream.close();

	}

	public static void main(String[] args) throws IOException {
		String nom;
		System.out.println("Quina opci� vols?");
		System.out.println("1.Comprimir una imatge");
		System.out.println("2.Descomprimir una imatge");
		Scanner sc = new Scanner(System.in);
		int opcion = sc.nextInt();
		nom = sc.nextLine();
		switch (opcion) {
		case 1:
			System.out.println("Vol comprimir sense perdues(0) o amb perdues(1)");
			opcion = sc.nextInt();
			nom = sc.nextLine();
			switch (opcion) {
			case 0:
				System.out.println("Indica el nombre de l'arxiu que vols comprimir");
				nom = sc.nextLine();
				comprimirImatge(nom);
				break;
			case 1:
				System.out.println("Indica el nombre de l'arxiu que vols comprimir");
				nom = sc.nextLine();
				comprimirImatgeAmbPerdues(nom);
				break;
			}
			break;
		case 2:
			System.out.println("Quina imatge vols descomprimir?");
			nom = sc.nextLine();
			System.out.println("Com vols vols que s'anomeni?");
			String nomNou = sc.nextLine();
			descomprimirImatge(nom, nomNou);
			break;
		}
		sc.close();
	}

}
