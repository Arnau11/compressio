import java.util.Arrays;
import java.util.Scanner;

public class RLE {
	private int array[];
	private int nElem;
	private boolean codificat;

	public RLE(int[] array) {
		this.array = array;
		nElem = array.length;
		codificat = false;
	}

	public RLE(int[] array, boolean cod) {
		this.array = array;
		nElem = array.length;
		codificat = cod;
	}

	@Override
	public String toString() {
		return "RLE [array=" + Arrays.toString(array) + "]";
	}

	public int getArray(int i) {
		return array[i];
	}

	public void setArray(int[] array) {
		this.array = array;
	}

	public int getnElem() {
		return nElem;
	}

	public void setnElem(int nElem) {
		this.nElem = nElem;
	}

	public boolean isCodificat() {
		return codificat;
	}

	public void setCodificat(boolean codificat) {
		this.codificat = codificat;
	}
	
	public void codificaRLEAmbPerdues() {
		int suma = 0;
		int arrayCod[] = new int[nElem * 2 + 1];
		int j = 0;
		int i = 0;
		for (i = 0; i < (nElem - 1); i++) {
			if (array[i] == array[i + 1]) {
				if (suma < 254) {
					suma++;
				} else {
					suma++;
					arrayCod[j] = suma;
					j++;
					arrayCod[j] = (array[i]/16);
					j++;
					suma = 0;
				}
			} else {
				suma++;
				arrayCod[j] = suma;
				j++;
				arrayCod[j] = (array[i]/16);
				j++;
				suma = 0;
			}
		}
		if (array[i] == array[i - 1]) {
			suma++;
			arrayCod[j] = suma;
			j++;
			arrayCod[j] = (array[i]/16);
			j++;
		} else {
			//arrayCod[j] = suma;
			//j++;
			//arrayCod[j] = array[i - 1];
			//j++;
			suma = 1;
			arrayCod[j] = suma;
			j++;
			arrayCod[j] = (array[i]/16);
			j++;
		}
		array = arrayCod;
		codificat = true;
	}

	public void codificaRLE() {
		int suma = 0;
		int arrayCod[] = new int[nElem * 2 + 1];
		int j = 0;
		int i = 0;
		for (i = 0; i < (nElem - 1); i++) {
			if (array[i] == array[i + 1]) {
				if (suma < 254) {
					suma++;
				} else {
					suma++;
					arrayCod[j] = suma;
					j++;
					arrayCod[j] = array[i];
					j++;
					suma = 0;
				}
			} else {
				suma++;
				arrayCod[j] = suma;
				j++;
				arrayCod[j] = array[i];
				j++;
				suma = 0;
			}
		}
		if (array[i] == array[i - 1]) {
			suma++;
			arrayCod[j] = suma;
			j++;
			arrayCod[j] = array[i];
			j++;
		} else {
			//arrayCod[j] = suma;
			//j++;
			//arrayCod[j] = array[i - 1];
			//j++;
			suma = 1;
			arrayCod[j] = suma;
			j++;
			arrayCod[j] = array[i];
			j++;
		}
		array = arrayCod;
		codificat = true;
	}

	public void descodificaRLE(int mida) { // mida de la foto picWidth
		int i = 0;
		int valor;
		int quantitat;
		int pos = 0;
		int arrayCod[] = new int[mida];
		while (i < array.length) {
			quantitat = array[i];
			valor = array[i + 1];
			for (int k = 0; k < quantitat; k++) {
				arrayCod[pos] = valor;
				pos++;
			}
			i = i + 2;
		}
		array = arrayCod;
		codificat = false;
		nElem = arrayCod.length;
	}
}