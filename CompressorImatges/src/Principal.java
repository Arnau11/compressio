
import java.io.*;
import java.util.Scanner;

public class Principal {
	
	private static int picWidth;
	private static int picHeight;
	
	
	
	private static int [][] imatgeAMatriu(String nomArxiu) throws IOException {
		String filePath = nomArxiu;
		FileInputStream fileInputStream = new FileInputStream(filePath);
		

		Scanner scan = new Scanner(fileInputStream);
		
		// Discard the magic number
		scan.skip("P5");
		
		// Discard the comment line
		//scan.nextLine();
		// Read pic width, height and max value
		 Principal.picWidth = scan.nextInt();

		 Principal.picHeight = scan.nextInt();
		
		int maxvalue = scan.nextInt();
	
		fileInputStream.close();
		scan.close();

		 // Now parse the file as binary data
		 fileInputStream = new FileInputStream(filePath);
		 DataInputStream dis = new DataInputStream(fileInputStream);
		 //BufferedReader d = new BufferedReader(new FileReader(filePath));

		 // look for 4 lines (i.e.: the header) and discard them
		 int numnewlines = 1;
		 while (numnewlines > 0) {
		     char c;
		     do {
		         c = (char)(dis.readUnsignedByte());
		     } while (c != '\n');
		     numnewlines--;
		 }
		 
		 // read the image data
		 int[][] data2D = new int[picHeight][picWidth];
		//try {
		 for (int row = 0; row < picHeight; row++) {
		     for (int col = 0; col < picWidth; col++) {
		         data2D[row][col] = dis.readUnsignedByte();
		       //  System.out.print(data2D[row][col] + " ");
		     }
		     System.out.println("fi"+row);
		 }
		 /*} catch (EOFException e) {
			 System.out.println("*************ERROR*******************");
			 
		 }*/
		
		 
		 dis.close();
		 return data2D;
	}
	
	private static int[] filaAArray (int fila, int[][] matriu) {
		int[] aux = new int[picWidth];
		for (int i=0; i<picWidth; i++) {
			aux[i]=matriu[fila][i];
		}
		
		for(int i=0; i<picWidth;i++) {
			System.out.println(aux[i]+" "+i);
		}
		return aux;
	}
	
	public static void main(String[] args) throws IOException {
		
		String nom = "prova.pgm";
		int[][] mat = imatgeAMatriu(nom);
		int aux[] = filaAArray(408,mat);
		RLE filacodificada= new RLE(aux);
		filacodificada.codificaRLE();
	}
	

}
