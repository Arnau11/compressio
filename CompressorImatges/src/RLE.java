public class RLE {
	private int array [];
	private int nElem;
	private boolean codificat;

	public RLE(int[] array) {
		this.array = array;
		nElem=array.length;
		codificat=false;
	}
	
	
	public int[] getArray() {
		return array;
	}



	public void setArray(int[] array) {
		this.array = array;
	}



	public int getnElem() {
		return nElem;
	}



	public void setnElem(int nElem) {
		this.nElem = nElem;
	}



	public boolean isCodificat() {
		return codificat;
	}



	public void setCodificat(boolean codificat) {
		this.codificat = codificat;
	}



	public void codificaRLE () {
		int suma=0;
		int arrayCod[]=new int[nElem];
		int j=0;
		int i=0;
		for(i=0; i<(nElem-1); i++) {
			if (array[i]==array[i+1]) {
				suma++;
			}
			else {
				suma++;
				arrayCod[j]= suma;
				j++;
				arrayCod[j]=array[i];
				j++;
				suma=0;
			}
		}
		if (array[i]==array[i-1]) {
			suma++;
			arrayCod[j]= suma;
			j++;
			arrayCod[j]=array[i];
			j++;
		}
		else {
			arrayCod[j]= suma;
			j++;
			arrayCod[j]=array[i-1];
			j++;
			suma=1;
			arrayCod[j]= suma;
			j++;
			arrayCod[j]=array[i];
			j++;
		}
	}
}